//// Object routines ////
#ifndef OBJECT_H
#define OBJECT_H

#include <stdbool.h>
#include "prop.h"

struct object;

void object_free(struct object *o);
struct object *object_new(void);
void object_retain(struct object *o);
void object_release(struct object *o);
int object_set_id(struct object *o, const char *id);
const char *object_get_id(const struct object *o);
const char *object_prop_get(struct object *o, const char *name);
int object_prop_set(struct object *o, const char *name, const char *value);
void object_prop_iter_init(const struct object *o, struct prop_iter *iter);
struct object *object_load(const char *filename);
int object_save(struct object *o, const char *filename);
int object_dump(const struct object *o);

/* Legacy API */
#define obj_new object_new
#define obj_release object_release
#define obj_get object_prop_get
#define obj_set object_prop_set

#endif
