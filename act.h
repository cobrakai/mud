#ifndef ACT_H
#define ACT_H
#include "tt.h"

struct act_context {
	struct connection *c; /* connection for input/output */
	struct object *o; /* object being executed by action */
	struct object *env; /* object associated with player connection */
	tt_state state; /* see tt.h */
};

#define ACT_DONE (TT_STATUS_EXITED) /* return when an action has completed */
#define ACT_BLOCKING (0x80000000) /* DO NOT USE! */

int actions_register(void);
#endif
