//// String Library ////
#ifndef STRING_LIB_H
#define STRING_LIB_H
struct string;

struct string *string_new(unsigned maximum_size);
int string_grow(struct string *s, unsigned new_size);
int string_addch(struct string *s, char ch);
int string_addwch(struct string *s, unsigned long w);
int string_set(struct string *s, const char *str);
const char *string_get(struct string *s);
int string_append(struct string *s, struct string *b);
struct string *string_dup(struct string *s);
void string_free(struct string *s);
void test_string(void);
#endif
