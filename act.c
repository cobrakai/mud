/* Copyright 2015-2017 Jon Mayo <jon@cobra-kai.com>
 *
 * Permission to use, copy, modify, or distribute this software for any
 * purpose with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/******************************************************************************/
#include <string.h>
#include "connection.h"
#include "cmd.h"
#include "objdb.h"
#include "object.h"
#include "rc.h"
#include "user.h"
#include "act.h"

/******************************************************************************/

// BUG: these functions print prompts twice because they need to re-enter at a
// different state. Use tt.h style pseudothreads to re-enter. return 0
// (ACT_DONE) or the state/context (line number) to be passed in later.

static int act_print(void *p)
{
	struct act_context *ctx = p;
	// struct object *ply_env = ctx->env;
	struct connection *c = ctx->c;
	struct object *obj = ctx->o;

	fprintf(stderr, "%s():p=%p\n", __func__, p);

	const char *message = obj_get(obj, "message");
	connection_printf(c, "%s", message);
	return ACT_DONE;
}

static int act_login(void *p)
{
	struct act_context *ctx = p;
	struct connection *c = ctx->c;
	tt_state *tts = &ctx->state;

	fprintf(stderr, "%s():%d:[fd%d] tts=%d buflen=%d\n", __func__, __LINE__, c->sockbase.fd, TT_LINE(tts), c->buflen);
	TT_BEGIN(tts)
	connection_printf(c, "LOGIN:");
	fprintf(stderr, "%s():%d:[fd%d] printing login prompt...\n", __func__, __LINE__, c->sockbase.fd);

	char username[64];
	TT_WAIT_WHILE(tts, connection_getline(c, username, sizeof(username)) == -1);
	/* note: username[] storage only valid until next TT_WAIT_WHILE */

	struct object *user = user_load(username);
	if (user) {
		connection_printf(c, "\nsuccess!\n");

		// set "current" to the next step if the username was found
		const char *success = obj_get(ctx->o, "success");
		obj_set(ctx->env, "current", success);

		fprintf(stderr, "%s():%d:[fd%d] current=\"%s\"\n", __func__, __LINE__, c->sockbase.fd, success);
		// it's critical that we return ACT_DONE if we've changed to the next state
	} else {
		// TODO: else print an error message
		connection_printf(c, "\n<INVALID USER>\n");
	}
	fprintf(stderr, "%s():%d:[fd%d] ACT_DONE\n", __func__, __LINE__, c->sockbase.fd);
	TT_END(tts);
}

static int act_command(void *p)
{
	struct act_context *ctx = p;
	struct connection *c = ctx->c;
	char line[2048];
	tt_state *tts = &ctx->state;
	int linelen;

	TT_BEGIN(tts)
	connection_printf(c, "\nC:\\> "); // TODO: make a configurable prompt
	TT_WAIT_WHILE(tts, (linelen = connection_getline(c, line, sizeof(line))) == -1);
	/* line[] and linelen are only valid until the next TT_ call. */
	if (linelen == 0) {
		connection_printf(c, "Ignoring empty line\n");
	} else {
		connection_printf(c, "\nline=\"%s\"\n\n", line);
		connection_printf(c, "I don't know what to do next!\n");
	}


	TT_END(tts);
}

int actions_register(void)
{
	/* load core commands */
	command_register("print", act_print);
	command_register("login", act_login);
	command_register("command", act_command);

	return 0;
}
