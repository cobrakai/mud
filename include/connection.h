#ifndef CONNECTION_H
#define CONNECTION_H
#include <stdarg.h>
#include <stddef.h>

#include "socket.h"
#include "telnet.h"

struct connection {
	struct sockbase sockbase;
	struct telnet telnet;
	/* input buffer */
	unsigned buflen, bufmax;
	char buf[512];
	/* output buffer */
	unsigned outbuf_len, outbuf_max;
	char outbuf[16384];
};

void connection_init(struct connection *c, SOCKET fd);
int connection_write(struct connection *c, const void *data, unsigned count);
int connection_vprintf(struct connection *c, const char *fmt, va_list ap);
int connection_printf(struct connection *c, const char *fmt, ...);
int connection_getline(struct connection *c, char *buf, size_t max);
#endif
