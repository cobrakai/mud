#ifndef SOCKET_H
#define SOCKET_H

/* flags for struct socket->event() */
#define EVENT_READ (1)
#define EVENT_WRITE (2)

typedef int SOCKET;
#define INVALID_SOCKET (-1)

struct sockbase {
	SOCKET fd;
	int rc;
	void (*event)(SOCKET fd, struct sockbase *ptr, long event);
	void (*free)(void *ptr);
};

void sockerror(const char *reason);
void sockclose(SOCKET fd);
int sockset(SOCKET fd, long events);
int sockclr(SOCKET fd, long events);
int sockadd(SOCKET fd, struct sockbase *ptr, long events, void (*event)(SOCKET fd, struct sockbase *ptr, long event), void (*free)(struct sockbase *ptr));
int sockpoll(void);

extern int sockets_count;
#endif
