#ifndef TELNET_H
#define TELNET_H

enum telnet_state {
	TS_NORMAL, TS_NORMAL_CR, TS_IAC, TS_DONT, TS_DO, TS_WONT, TS_WILL,
	TS_SB, TS_SUBNEGOTIATION, TS_SUBNEGOTIATION_IAC,
};

struct telnet {
	enum telnet_state state;
	unsigned option; /* current option - usually for subnegotiation */
	char sub[1024]; /* subnegotiation buffer */
	unsigned sub_len, sub_max;
};

/* provides callbacks and working variables */
struct telnet_context {
	struct connection *c;
	void *p;
	void (*f_will)(struct connection *c, void *p, unsigned char telopt);
	void (*f_wont)(struct connection *c, void *p, unsigned char telopt);
	void (*f_do)(struct connection *c, void *p, unsigned char telopt);
	void (*f_dont)(struct connection *c, void *p, unsigned char telopt);
	void (*f_sub)(struct connection *c, void *p, unsigned char telopt, const char *sub, unsigned sub_len);
};


/* telnet.c */
int telnet_parse(struct telnet *telnet, const struct telnet_context *ctx, const char *inbuf, unsigned inbuf_len, char *outbuf, unsigned outbuf_max);
#endif
