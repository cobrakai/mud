/* Copyright 2015-2017 Jon Mayo <jon@cobra-kai.com>
 *
 * Permission to use, copy, modify, or distribute this software for any
 * purpose with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "filedb.h"
#include "object.h"
#include "rc.h"

int main()
{
	mkdir("testdb", 0777);
	filedb_set_root("testdb");

	/* test code */
	struct object *a = object_new();
	object_set_id(a, "obj1");
	char *data[] = {
	"happy", "joy",
	"a", "100", "b", "200", "c", "300",
	"g", "700", "h", "800", "i", "900",
	"test", "1", "flag", "",
	"j", "1000", "k", "1100", "l", "1200",
	"m", "1300", "n", "1400",
	"d", "400", "e", "500", "f", "600",
	NULL, };
	int i;

	fprintf(stderr, "TEST1: (expect nil) %p\n", object_prop_get(a, "happy"));
	for (i = 0; data[i]; i += 2) {
		fprintf(stderr, "  \"%s\"=\"%s\"\n", data[i], data[i + 1]);
		object_prop_set(a, data[i], data[i + 1]);
	}
	fprintf(stderr, "TEST2: (expect \"joy\") \"%s\"\n", object_prop_get(a, "happy"));
	fprintf(stderr, "TEST3: (expect \"\") \"%s\"\n", object_prop_get(a, "flag"));
	fprintf(stderr, "TEST4: (expec \"1\") \"%s\"\n", object_prop_get(a, "test"));

	const char test_path[] = "obj1";
	{
		/* saving test */
		int e = object_save(a, test_path);
		if (e == -1) {
			fprintf(stderr, "%s():error!\n", "object_save");
			return EXIT_FAILURE;
		}
		obj_release(a);
	}

	{
		/* loading test */
		struct object *b = object_load(test_path);
		if (!b) {
			fprintf(stderr, "%s():%s:error!\n", "object_load", test_path);
			return EXIT_FAILURE;
		}

		/* dump the object */
		object_dump(b);

		obj_release(b);
	}

	return 0;
}
