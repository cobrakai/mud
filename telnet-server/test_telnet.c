/* Copyright 2015-2017 Jon Mayo <jon@cobra-kai.com>
 *
 * Permission to use, copy, modify, or distribute this software for any
 * purpose with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "connection.h"
#include "telnet.h"

/* can be placed in a string literal without escapes */
#define is_unquote(c) (isgraph(c) && c != '"')

/* output in a diagnostic fashion: "printable" <non-printable> */
static void dump(FILE *f, size_t n, const void *p)
{
	const unsigned char *s = p;
	size_t i;

	while (n) {
		for (i = 0; i < n && is_unquote(s[i]); i++) ;
		if (i) {
			fprintf(f, " \"%.*s\"", (int)i, s);
			s += i;
			n -= i;
		}
		for (i = 0; i < n && !is_unquote(s[i]); i++) ;
		if (i) {
			fprintf(f, " <");
			while (i) {
				fprintf(f, "%hhu", *s);
				s++;
				n--;
				i--;
				if (i)
					fprintf(f, " ");
			}
			fprintf(f, ">");
		}
	}
	fprintf(f, "\n");
}

static void my_dont(struct connection *c, void *p, unsigned char telopt)
{
	fprintf(stderr, "IAC DONT %hhu\n", telopt);
}

int main()
{
	/* test code */
	char output[1024];

	struct telnet_context ctx = {
		.c = NULL,
		.p = NULL,
		.f_dont = my_dont,
	};

	struct telnet state;
	memset(&state, 0, sizeof(state));

	const char buf_a[] = { 'H', 'i', 255, 255, 255, 254, 1 }; /* Hi <IAC IAC> <IAC DONT ECHO> */
	const char buf_b[] = { ' ', 'W', 'o', 'r', 'l', 'd', '\r', '\n', };

	struct { size_t n; const char *p; } pieces[] = {
		{ 0, (char[]){ } },
		{ 3, (char[]){ 255, 251, 1 } },
		{ 1, (char[]){ 255, } },
		{ 1, (char[]){ 255, } },
		{ sizeof(buf_a), buf_a },
		{ sizeof(buf_b), buf_b },
		{ 6, "hello " },
		{ 6, "world\n" },
		{ 9, "\33[0;1;32m" }, /* ESC [ 0 m      */
		{ 2, (char[]){ 255, 255, }}, /* IAC IAC */
		{ 3, (char[]){ 255, 251, 1 }}, /* IAC WILL TELOPT_ECHO */
		{ 12, "\377\364\377\365\377\366\377\367\377\370\377\371" }, /* IAC IP IAC AO IAC AYT IAC EC IAC EL IAC GA */
		{ 3, "\377\361\377" }, /* IAC NOP IAC */
		{ 2, "\376\42" }, /* DONT TELOPT_LINEMODE */
		{ 15, "this is a test\n" },
		{ 2, "\377\377" }, /* IAC IAC */
		{ 4, "\33[0m" }, /* ESC [ 0 m  */
		{ 2, "\377\377" }, /* IAC IAC */
		{ 4, "\377\372\42\1" }, /* IAC SB LINEMODE MODE */
		{ 3, "\1\377\360" }, /* EDIT IAC SE */
		{ 4, "y\377\360x" }, /* y IAC SE x */
		{ 0, NULL }
	};

	unsigned i;
	for (i = 0; pieces[i].p; i++) {
		fprintf(stderr, "part%u: *****\n", i);
		int len = telnet_parse(&state, &ctx, pieces[i].p, pieces[i].n, output, sizeof(output));
		if (len > 0) {
			printf("part%u:OUT:", i);
			dump(stdout, len, output);
		} else if (len == 0) {
			printf("part%u:OUT:<empty>\n", i);
		} else {
			fprintf(stderr, "part%u:ERROR! (%d)\n", i, len);
			return 1;
		}
	}

	return 0;
}
