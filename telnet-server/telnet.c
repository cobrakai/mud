/* Copyright 2015-2017 Jon Mayo <jon@cobra-kai.com>
 *
 * Permission to use, copy, modify, or distribute this software for any
 * purpose with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/******************************************************************************/
#include <stdio.h>

#include "telnet.h"

/******************************************************************************/
/* TELNET protocol */

/* process incoming telnet data */
int telnet_parse(struct telnet *telnet, const struct telnet_context *ctx, const char *inbuf, unsigned inbuf_len, char *outbuf, unsigned outbuf_max)
{
	unsigned a, b;

	for (a = 0, b = 0; a < inbuf_len; a++) {
		if (b >= outbuf_max)
			return -1; /* terrible error */
		unsigned char ch = inbuf[a];
		switch (telnet->state) {
		case TS_NORMAL:
			switch (ch) {
			case 255: /* if ch == IAC (255) then change state */
				telnet->state = TS_IAC;
				break;
			case '\r': /* filter CR LF -> newline */
				telnet->state = TS_NORMAL_CR;
				outbuf[b++] = '\n';
				break;
			default:
				outbuf[b++] = ch;
			}
			break;
		case TS_NORMAL_CR: /* this state is to filter CR LF -> newline */
			switch (ch) { /* if ch == IAC (255) then change state */
			case 255:
				telnet->state = TS_IAC;
				break;
			case '\n':
			case 0:
				/* ignore LF and NUL, as TS_NORMAL handled it */
				telnet->state = TS_NORMAL;
				break;
			default:
				telnet->state = TS_NORMAL; /* return to TS_NORMAL state */
				outbuf[b++] = ch;
			}
			break;
		case TS_IAC:
			fprintf(stderr, "%s():%d:IAC\n", __func__, __LINE__);
			telnet->state = TS_NORMAL; /* default to leaving to TS_NORMAL state */
			switch (ch) {
			case 255: /* IAC IAC -> IAC */
				fprintf(stderr, "%s():%d:IAC IAC\n", __func__, __LINE__);
				outbuf[b++] = 255;
				telnet->state = TS_NORMAL;
				break;
			case 254:
				fprintf(stderr, "%s():%d:IAC DONT\n", __func__, __LINE__);
				telnet->state = TS_DONT;
				break;
			case 253:
				fprintf(stderr, "%s():%d:IAC DO\n", __func__, __LINE__);
				telnet->state = TS_DO;
				break;
			case 252:
				fprintf(stderr, "%s():%d:IAC WONT\n", __func__, __LINE__);
				telnet->state = TS_WONT;
				break;
			case 251:
				fprintf(stderr, "%s():%d:IAC WILL\n", __func__, __LINE__);
				telnet->state = TS_WILL;
				break;
			case 250:
				fprintf(stderr, "%s():%d:IAC SB\n", __func__, __LINE__);
				telnet->state = TS_SB;
				break;
			case 244:
				fprintf(stderr, "TODO:%s():TELNET interrupt (IP)\n", __func__);
				telnet->state = TS_NORMAL;
				break;
			default:
				fprintf(stderr, "ERROR:%s():unknown TELNET code %d\n", __func__, ch);
				telnet->state = TS_NORMAL;
			}
			break;
		case TS_DONT:
			telnet->state = TS_NORMAL;
			fprintf(stderr, "%s():%d:IAC DONT %hhu\n", __func__, __LINE__, ch);
			if (ctx->f_dont)
				ctx->f_dont(ctx->c, ctx->p, ch);
			else
				fprintf(stderr, "TODO:%s():TELNET DONT %d\n", __func__, ch);
			break;
		case TS_DO:
			telnet->state = TS_NORMAL;
			fprintf(stderr, "%s():%d:IAC DO %hhu\n", __func__, __LINE__, ch);
			if (ctx->f_do)
				ctx->f_do(ctx->c, ctx->p, ch);
			else
				fprintf(stderr, "TODO:%s():TELNET DO %d\n", __func__, ch);
			break;
		case TS_WONT:
			telnet->state = TS_NORMAL;
			fprintf(stderr, "%s():%d:IAC WONT %hhu\n", __func__, __LINE__, ch);
			if (ctx->f_wont)
				ctx->f_wont(ctx->c, ctx->p, ch);
			else
				fprintf(stderr, "TODO:%s():TELNET WONT %d\n", __func__, ch);
			break;
		case TS_WILL:
			telnet->state = TS_NORMAL;
			fprintf(stderr, "%s():%d:IAC WILL %hhu\n", __func__, __LINE__, ch);
			if (ctx->f_will)
				ctx->f_will(ctx->c, ctx->p, ch);
			else
				fprintf(stderr, "TODO:%s():TELNET WILL %d\n", __func__, ch);
			break;
		case TS_SB:
			fprintf(stderr, "%s():%d:IAC SB %hhu\n", __func__, __LINE__, ch);
			telnet->option = ch;
			telnet->state = TS_SUBNEGOTIATION;
			break;
		case TS_SUBNEGOTIATION:
			if (ch == 255)
				telnet->state = TS_SUBNEGOTIATION_IAC;
			/* insert into the subnegotiation buffer */
			else if (telnet->sub_len < telnet->sub_max)
				telnet->sub[telnet->sub_len++] = ch;
			break;
		case TS_SUBNEGOTIATION_IAC:
			fprintf(stderr, "%s():%d:SB .. IAC %hhu\n", __func__, __LINE__, ch);
			switch (ch) {
			case 255: /* IAC IAC -> IAC */
				/* insert a 255 into the subnegotation buffer */
				if (telnet->sub_len < telnet->sub_max)
					telnet->sub[telnet->sub_len++] = 255;
				break;
			case 240: /* IAC SE */
				/* process the subnegotiation buffer */
				if (ctx->f_sub)
					ctx->f_sub(ctx->c, ctx->p, telnet->option, telnet->sub, telnet->sub_len);
				else
					fprintf(stderr, "TODO:%s():TELNET SB %d .. IAC SE\n", __func__, telnet->option);
				telnet->sub_len = 0;
				telnet->state = TS_NORMAL;
				break;
			default:
				fprintf(stderr, "ERROR:%s():illegal TELNET protocol (subnegotiation).\n", __func__);
			}
			break;
		}
	}
	return b;
}
