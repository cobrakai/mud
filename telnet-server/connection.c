/* Copyright 2015-2017 Jon Mayo <jon@cobra-kai.com>
 *
 * Permission to use, copy, modify, or distribute this software for any
 * purpose with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/******************************************************************************/
#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "connection.h"

/******************************************************************************/
/* connection stream - can be used by servers or clients */

void connection_init(struct connection *c, SOCKET fd)
{
	c->sockbase.fd = fd;
	c->buflen = 0;
	c->bufmax = sizeof(c->buf); // TODO: support dynamic allocation
	c->outbuf_len = 0;
	c->outbuf_max = sizeof(c->outbuf); // TODO: support dynamic allocation
}

/* connection_write() may return a short count.
 * NOTE: this function does not escape TELNET codes.
 * return -1 if the output buffer is already full or socket is disconnected.
 */
int connection_write(struct connection *c, const void *data, unsigned count)
{
	int rem = (int)c->outbuf_max - (int)c->outbuf_len;
	if (rem <= 0 || c->sockbase.fd == INVALID_SOCKET)
		return -1;
	if (count > (unsigned)rem)
		count = rem;
	memcpy(c->outbuf + c->outbuf_len, data, count);
	c->outbuf_len += count;
	sockset(c->sockbase.fd, EVENT_WRITE);
	return count;
}

/* connection_vprintf() performs a vprintf into the output buffer.
 * Escapes telnet codes in output string.
 * return -1 if the output buffer is already full or socket is disconnected or
 * when there is a buffer overflow.
 */
int connection_vprintf(struct connection *c, const char *fmt, va_list ap)
{
	static __thread unsigned char decode_buf[1048576];
	int rem = (int)c->outbuf_max - (int)c->outbuf_len;
	if (rem <= 0 || c->sockbase.fd == INVALID_SOCKET)
		return -1;
	int e = vsnprintf((char*)decode_buf, sizeof(decode_buf), fmt, ap);
	if (e > (int)sizeof(decode_buf) - 1) {
			fprintf(stderr, "WARNING:%s():output truncated (rem=%d e=%d)\n", __func__, rem, e);
			return -1;
	}

	int i;
	unsigned char ch;
	for (i = 0; (ch = decode_buf[i]); i++) {
		switch (ch) {
		case '\n':
			if (c->outbuf_max <= c->outbuf_len + 2) {
				fprintf(stderr, "WARNING:%s():output truncated (max=%d len=%d)\n", __func__, c->outbuf_max, c->outbuf_len);
				return -1;
			}
			c->outbuf[c->outbuf_len++] = '\r';
			c->outbuf[c->outbuf_len++] = '\n';
			break;
		case 255:
			if (c->outbuf_max <= c->outbuf_len + 2) {
				fprintf(stderr, "WARNING:%s():output truncated (max=%d len=%d)\n", __func__, c->outbuf_max, c->outbuf_len);
				return -1;
			}
			c->outbuf[c->outbuf_len++] = 255;
			c->outbuf[c->outbuf_len++] = 255;
			break;
		default:
			if (c->outbuf_max <= c->outbuf_len + 1) {
				fprintf(stderr, "WARNING:%s():output truncated (max=%d len=%d)\n", __func__, c->outbuf_max, c->outbuf_len);
				return -1;
			}
			c->outbuf[c->outbuf_len++] = ch;
			break;
		}
	}
	sockset(c->sockbase.fd, EVENT_WRITE);
	return e;
}

int connection_printf(struct connection *c, const char *fmt, ...) __attribute__ ((format (printf, 2, 3)));
int connection_printf(struct connection *c, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	int e = connection_vprintf(c, fmt, ap);
	va_end(ap);
	return e;
}

/* returns a line of text from the input buffer
 * return -1 when no line is available or on error.
 * return 0 for an empty line.
 * else return the length of the line that has been consumed (may be larger than max). */
int connection_getline(struct connection *c, char *buf, size_t max)
{
	if (c->sockbase.fd == INVALID_SOCKET) {
		c->buflen = 0; /* discard remaining input */
		return -1;
	}
	if (!c->buflen) {
		return -1;
	}

	/* telnet code above us is already translating CR LF -> LF */
	char *end = memchr(c->buf, '\n', c->buflen);
	fprintf(stderr, "%s():%d:end=%p buflen=%d\n", __func__, __LINE__, end, c->buflen);
	if (!end)
		return -1;
	*end = 0;

	/* copy the line without including a newline */
	unsigned len = end - c->buf;
	if (len >= max - 1)
		len = max - 1; /* truncate, with room for null terminator */
	memcpy(buf, c->buf, len);
	buf[len] = 0;

	/* consume the line, including the LF */
	end++;
	int rem = c->buflen - (end - c->buf);
	if (rem > 0) {
		memmove(c->buf, end, rem);
		c->buflen = rem;
	} else {
		c->buflen = 0;
	}

	fprintf(stderr, "[fd%d] line=\"%.*s\"\n", c->sockbase.fd, len, buf);
	fprintf(stderr, "[fd%d] buflen=%d\n", c->sockbase.fd, c->buflen);

	return len;
}
