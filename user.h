#ifndef USER_H
#define USER_H
struct object *user_load(const char *username);
struct object *user_create(const char *username);
#endif
