#!/usr/bin/env make
# minibuild: a make-based build system
# version : 23-Aug-2017
#
##
# Features:
# - target filename does not have to match one of the source filenames
# - handles sub-directories in a convenient way
# - run 'make install' to install all components into INSTALL_DIR
# - build settings can be global or per-component. (somewhat limited)
#
# Usage:
# - list configuration in project.mk
# - run 'make' for default rule.
# - run 'make install' to install into a bin/ directory.
# - define INSTALL_DIR in project.mk or on command-line for installation path.
#
# Methods:
# SubDir - recurse into a sub-directory. Uses component.mk instead of project.mk
# Executable - create an executable for target.
# Module - create a DLL/plugin
# StaticLibrary - create a static library / archive.
#
# Examples:
#
# $(call Executable,launcher,launcher.c,,,)
# $(call Executable,telserv,telserv.c telnet.c,,-pthread,-lm -lev)
# $(call Executable,eventserv,eventserv.c eventproto.c event.c,,-pthread,-lm -lev)
# $(call Executable,authserv,authserv.c authproto.c dbproto.c,,-pthread,-lm -lev)
# $(call Executable,dbserv,dbserv.c dbproto.c,,-pthread,-lm -lev)
# $(call Executable,basicserv,basicserv.c,,-pthread,-lm -lev)
# $(call StaticLibrary,net,net.c,-pthread)
# $(call SubDir,yourdir)

all ::
clean ::
help ::
	@echo "all     - build everything. default"
	@echo "clean   - clean everything."
	@echo "install - install into $(INSTALL_DIR)"
	@echo "help    - this manual."
##

_topdir = $(dir $(firstword $(MAKEFILE_LIST)))
_subdir = $(dir $(lastword $(MAKEFILE_LIST)))

##

StaticLibraryName = $(1).a

# _ObjFilter = $$(filter %.o,$$(patsubst %.S,%.o,$$(patsubst %.c,%.o,$$(S.$(1)))))
_ObjFilter = $$(addprefix $$(_subdir),$$(filter %.o,$$(patsubst %.S,%.o,$$(patsubst %.c,%.o,$$(S.$(1))))))

define _NewExecutable
S.$(1) := $(2)
O.$(1) := $(call _ObjFilter,$(1))
L.$(1) := $$(foreach x,$(3),$$(call StaticLibraryName,$$x))
X.$(1) := $$(_subdir)$(1)
I.$(1) := $(INSTALL_DIR)/$(1)
$$(X.$(1)) : $$(O.$(1)) $$(L.$(1))
	$$(CC) $$(LDFLAGS) $$(TARGET_ARCH) -o $$@ $$^ $$(LDLIBS)
$$(I.$(1)) : $$(X.$(1)) ; install $$^ $$@
all :: $$(X.$(1))
clean :: ; $$(RM) $$(O.$(1)) $$(X.$(1))
install :: $$(I.$(1))
$(if $(4),$$(X.$(1)) : CFLAGS += $(4))
$(if $(5),$$(X.$(1)) : LDLIBS += $(5))
endef

# Parameters: executable,sources,libraries,cflags,ldlibs
Executable = $(eval $(call _NewExecutable,$(1),$(2),$(3),$(4),$(5)))

define _NewStaticLibrary
S.$(1) := $(2)
O.$(1) := $(call _ObjFilter,$(1))
L.$(1) := $(call StaticLibraryName,$(1))
$$(L.$(1)) : $$(L.$(1))($$(O.$(1)))
all :: $$(L.$(1))
clean :: ; $$(RM) $$(O.$(1)) $$(L.$(1))
$(if $(3),$$(L.$(1)) : CFLAGS += $(3))
endef

# Parameters: library_base,sources,cflags
StaticLibrary = $(eval $(call _NewStaticLibrary,$(1),$(2),$(3)))

# Parameters: directory
SubDir = $(eval include $(1)/component.mk)

# Parameters: module_name,sources,libraries,cflags,ldlibs
Module = $(eval $(call _NewModule,$(1),$(2),$(3),$(4),$(5)))

ModuleName = $(1).so

define _NewModule
S.$(1) := $(2)
O.$(1) := $(call _ObjFilter,$(1))
L.$(1) := $$(foreach x,$(3),$$(call StaticLibraryName,$$x))
M.$(1) := $$(_subdir)$(call ModuleName,$(1))
I.$(1) := $(INSTALL_DIR)/$(1)
$$(M.$(1)) : $$(O.$(1)) $$(L.$(1))
$$(I.$(1)) : $$(X.$(1)) ; install $$^ $$@
all :: $$(M.$(1))
clean :: ; $$(RM) $$(O.$(1)) $$(M.$(1))
$$(M.$(1)) : CFLAGS += -fPIC $(3)
$$(M.$(1)) : $$(O.$(1))
	$$(CC) $$(CFLAGS) $$(TARGET_ARCH) -shared -Wl,-soname,$$(M.$(1)) -o $$@ $$^
endef

##

include project.mk

##

INSTALL_DIR ?= $(realpath $(_topdir))/bin

$(INSTALL_DIR) :
	mkdir -p $(INSTALL_DIR)

install :: | $(INSTALL_DIR)

## END OF FILE ##
