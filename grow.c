/* Copyright 2015-2017 Jon Mayo <jon@cobra-kai.com>
 *
 * Permission to use, copy, modify, or distribute this software for any
 * purpose with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "grow.h"

int grow(void *ptr, unsigned *max, unsigned min, size_t elem)
{
	unsigned old = *max * elem;
	unsigned len = min * elem;

	if (len <= old)
		return 0; /* nothing to do */

	len--;
	len |= len >> 1;
	len |= len >> 2;
	len |= len >> 4;
	len |= len >> 8;
	len |= len >> 16;
	len++;
	void *newptr = realloc(*(char**)ptr, len);
	if (!newptr) {
		perror(__func__);
		return -1;
	}
	memset(newptr + old, 0, len - old);
	*max = len / elem;
	*(char**)ptr = newptr;
	return 0;
}
