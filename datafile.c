/* Copyright (c) 2016-2017, Jon Mayo <jon@cobra-kai.com> */
/* Data file lexicon and grammar.
 *
 * tokens:
 *	T_STRING
 *	T_EQUAL
 *	T_OPEN
 *	T_CLOSE
 *	T_EOF
 *
 * file := object* T_EOF
 * object := T_STRING T_OPEN property* T_CLOSE
 * property := T_STRING T_EQUAL T_STRING
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include "log.h"
#include "string-lib.h"
#include "datafile.h"

/* these are the terminals (except T_ERROR, which is just an error code) */
enum datafile_token {
	T_ERROR, /* this is just an error code */
	T_EOF, /* end-of-file */
	T_OPEN, /* { */
	T_CLOSE, /* } */
	T_STRING, /* "..." */
	T_EQUAL, /* = */
};

struct datafile {
	FILE *f;
	const char *filename;
	int linenum;
	int state;
	enum datafile_token token_type; /* type of value below */
	struct {
		struct string *str;
	} token;
};

/* creates an event stream */
struct datafile *
datafile_openfile(FILE *f, const char *filename)
{
	struct datafile *ds = calloc(1, sizeof(*ds));
	ds->f = f;
	ds->filename = filename;
	ds->linenum = 1;
	ds->token.str = string_new(0);
	return ds;
}

void
datafile_close(struct datafile *ds)
{
	ds->f = NULL;
	ds->filename = NULL;
	string_free(ds->token.str);
	ds->token.str = NULL;
	free(ds);
}

static inline int
ds_getc(struct datafile *ds)
{
	int c = fgetc(ds->f);
	if (c == '\n')
		ds->linenum++;
	return c;
}

/* returns EOF(-1) on error */
static inline int hex_value(int c)
{
	switch (c) {
	case '0': return 0;
	case '1': return 1;
	case '2': return 2;
	case '3': return 3;
	case '4': return 4;
	case '5': return 5;
	case '6': return 6;
	case '7': return 7;
	case '8': return 8;
	case '9': return 9;
	case 'a': case 'A': return 10;
	case 'b': case 'B': return 11;
	case 'c': case 'C': return 12;
	case 'd': case 'D': return 13;
	case 'e': case 'E': return 14;
	case 'f': case 'F': return 15;
	}
	return EOF;
}

static int
read_unicode(struct datafile *ds)
{
	int a = hex_value(ds_getc(ds));
	if (a == EOF)
		return -1;
	int b = hex_value(ds_getc(ds));
	if (b == EOF)
		return -1;
	int c = hex_value(ds_getc(ds));
	if (c == EOF)
		return -1;
	int d = hex_value(ds_getc(ds));
	if (d == EOF)
		return -1;
	unsigned long u =
		((unsigned long)a << 24) | ((unsigned long)b << 16) | ((unsigned long)c << 8) | d;
	return string_addwch(ds->token.str, u);
}

static int
read_escaped(struct datafile *ds)
{
	int c = ds_getc(ds);
	switch (c) {
	case EOF:
		return -1;
	case 'u':
		return read_unicode(ds);
	case '\\':
	case '/':
	case '"':
		return string_addch(ds->token.str, c);
	case 'n':
		return string_addch(ds->token.str, '\n');
	case 't':
		return string_addch(ds->token.str, '\t');
	case 'b':
		return string_addch(ds->token.str, '\b');
	case 'f':
		return string_addch(ds->token.str, '\f');
	case 'r':
		return string_addch(ds->token.str, '\r');
	}
	return -1; /* unknown escape */
}

static int
read_string(struct datafile *ds)
{
	while (1) {
		int c = ds_getc(ds);
		switch (c) {
		case EOF:
			return -1;
		case '"':
			// DEBUG: fprintf(stderr, "success! str=%s\n", string_get(ds->token.str));
			return 0;
		case '\\':
			if (read_escaped(ds))
				return -1;
			continue;
		default:
			if (string_addch(ds->token.str, c))
				return -1;
			/*
			log_debug("%s:%d:str=\"%s\"\n",
				ds->filename, ds->linenum,
				string_get(ds->token.str));
			*/
			continue;
		}
	}
}

static enum datafile_token
next_token(struct datafile *ds)
{
	while (1) {
		int c = ds_getc(ds);
		/*
		log_debug("%s:%d:ch=%d\n",
			ds->filename, ds->linenum, c);
		*/
		switch (c) {
		case EOF:
			return T_EOF;
		case ' ': case '\t': case '\n': case '\f': case '\r':
			continue;
		case '"':
			string_set(ds->token.str, NULL);
			if (read_string(ds))
				return T_ERROR;
			// DEBUG: fprintf(stderr, "%s():%d:str=\"%s\"\n", __func__, __LINE__, string_get(ds->token.str));
			return ds->token_type = T_STRING;
		case '=':
			return T_EQUAL;
		case '{':
			return T_OPEN;
		case '}':
			return T_CLOSE;
		}
	}
}

/* take the next event from the stream */
enum datafile_event
datafile_next(struct datafile *ds)
{
	while (1) {
		enum datafile_token tok = next_token(ds);
		/*
		log_debug("%s:%d:token=%d state=%d\n",
			ds->filename, ds->linenum, tok, ds->state);
		*/
		if (tok == T_ERROR)
			goto parse_error;
		/*
		if (tok == T_EOF)
			log_debug("%s:%d:EOF in state %d\n",
				ds->filename, ds->linenum, ds->state);
		*/
		// DEBUG: if (tok == T_STRING) fprintf(stderr, "%s():%d:tok=%d:str=\"%s\"\n", __func__, __LINE__, tok, string_get(ds->token.str));
		switch (ds->state) {
		case 0 : /* file := @ object* T_EOF */
			if (tok == T_EOF) {
				/*
				log_debug("%s:%d:DONE\n",
					ds->filename, ds->linenum);
				*/
				return D_DONE;
			}
			/* object := @ T_STRING T_OPEN property* T_CLOSE */
			else if (tok != T_STRING)
				goto parse_error;
			ds->state = 1;
			continue;
		case 1: /* object := T_STRING @ T_OPEN property* T_CLOSE */
			if (tok != T_OPEN)
				goto parse_error;
			ds->state = 2;
			/*
			log_info("%s:%d:TRACE:new object \"%s\"\n",
				ds->filename, ds->linenum,
				string_get(ds->token.str));
			*/
			return D_OBJECT_OPEN;
		case 2: /* object := T_STRING @ T_OPEN @ property* T_CLOSE */
			if (tok == T_CLOSE) {
				ds->state = 0;
				return D_OBJECT_CLOSE;
			} else if (tok != T_STRING) {
				goto parse_error;
			}
			ds->state = 3;
			return D_PROPERTY_NAME;
		case 3: /* property := {{ T_STRING @ T_EQUAL }} T_STRING */
			if (tok != T_EQUAL)
				goto parse_error;
			ds->state = 4;
			continue;
		case 4: /* property := T_STRING T_EQUAL @ {{ T_STRING }} */
			if (tok != T_STRING)
				goto parse_error;
			ds->state = 2;
			return D_PROPERTY_VALUE;
		default:
			log_error("%s:%d:illegal state %d\n",
				ds->filename, ds->linenum, ds->state);
			return D_ERROR;
		}
	}
parse_error:
	log_error("%s:%d:parse error\n", ds->filename, ds->linenum);
	return D_ERROR;
}

const char *
datafile_get_string(struct datafile *ds)
{
	return string_get(ds->token.str);
}

const char *
datafile_filename(struct datafile *ds)
{
	return ds->filename;
}

int
datafile_line(struct datafile *ds)
{
	return ds->linenum;
}

static int
write_escaped(FILE *f, const char *filename, const char *s)
{
	int c;
	while ((c = *s++)) {
#warning FIXME: UTF-8 sequences not correctly handled at this time.
		// TODO: process valid UTF-8 sequences into \u
		switch (c) {
		case '\\':
			fputs("\\\\", f);
			break;
		case '"':
			fputs("\\\"", f);
			break;
		case '\n':
			fputs("\\n", f);
			break;
		case '\t':
			fputs("\\t", f);
			break;
		case '\b':
			fputs("\\b", f);
			break;
		case '\f':
			fputs("\\f", f);
			break;
		case '\r':
			fputs("\\r", f);
			break;
		default:
			fputc(c, f);
		}
	}
	if (ferror(f)) {
		log_syserror(filename);
		return -1;
	}
	return 0;
}

int
datafile_write_start(FILE *f, const char *filename, const char *tag)
{

	fputs("\"", f);
	write_escaped(f, filename, tag);
	fputs("\" {\n", f);
	if (ferror(f))
		return -1;
	return 0;
}

int
datafile_write_property(FILE *f, const char *filename, const char *name, const char *value)
{
	fputs("\"", f);
	write_escaped(f, filename, name);
	fputs("\" = \"", f);
	write_escaped(f, filename, value);
	fputs("\"\n", f);
	if (ferror(f))
		return -1;
	return 0;
}

int
datafile_write_end(FILE *f, const char *filename)
{
	fputs("}\n", f);
	if (ferror(f)) {
		log_syserror(filename);
		return -1;
	}
	return 0;
}
