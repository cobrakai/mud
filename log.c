/* Copyright (c) 2016-2017, Jon Mayo <jon@cobra-kai.com> */

//// Logging ////

#include <stdio.h>
#include <stdarg.h>
#include "log.h"

static const char *log_names[] = { "ASSERT", "CRITIAL", "ERROR", "WARNING", "INFO", "TODO", "DEBUG", "TRACE" };

void
log_format(char *buf, size_t buflen, enum log_level level, const char *tag, const char *fmt, va_list ap)
{
	if (!buflen)
		return;
	unsigned n = 0; /* current used length of buf */
	unsigned r = buflen - 1; /* remaining - reserved space for null terminator */
	unsigned c;

	/* apply the log level string */
	const char *name = /* (level >= LOG_ASSERT) && */ (level <= LOG_TRACE) ?
		log_names[level] : "UNKNOWN";
	c = snprintf(buf + n, r, "%s:", name);
	if (c > r)
		goto error_truncated;
	n += c;
	r -= c;

	/* apply to the tag if it exists */
	if (tag) {
		c = snprintf(buf + n, r, "%s:", tag);
		if (c > r)
			goto error_truncated;
		n += c;
		r -= c;
	}

	/* format the message */
	c = vsnprintf(buf + n, r, fmt, ap);
	if (c > r)
		goto error_truncated;
	n += c;
	r -= c;

	/* add newline if one does not exist */
	if (n && buf[n - 1] != '\n') {
		if (!r)
			goto error_truncated;
		buf[n++] = '\n';
		buf[n] = 0;
		r--;
	}

	return;
error_truncated:
	snprintf(buf, buflen, "%s:%s():truncated log message! (%s)\n", log_names[LOG_CRIT], __func__, fmt);
}

void
log_msg(enum log_level level, const char *tag, const char *fmt, ...)
{
        char buf[128];
	va_list ap;
	va_start(ap, fmt);
	log_format(buf, sizeof(buf), level, tag, fmt, ap);
	va_end(ap);
	fputs(buf, stderr);
}

//// End of Logging ////
