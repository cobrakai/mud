/* Copyright 2015-2017 Jon Mayo <jon@cobra-kai.com>
 *
 * Permission to use, copy, modify, or distribute this software for any
 * purpose with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/******************************************************************************/
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "container.h"
#include "cmd.h"
#include "filedb.h"
#include "object.h"
#include "rc.h"
#include "socket.h"
#include "connection.h"
#include "tt.h"
#include "act.h"

/******************************************************************************/

#define DLIST_INSERT_AFTER(head, item) do { \
	(item)->prev = (head); \
	(item)->next = *(head); \
	*(head) = (item); \
	} while (0)
#define DLIST_REMOVE(item) do { \
	if ((item)->prev) *(item)->prev = (item)->next; \
	(item)->prev = NULL; \
	(item)->next = NULL; \
	} while (0)

/******************************************************************************/

struct object *system_env; /* system environment options */

/******************************************************************************/
/* each remote client is serviced by a single server instance */
struct server {
	struct connection c;
	struct server *next, **prev;
	struct object *env; /* current environment */
	tt_state tt_state;
};

static void server_close(struct server *s)
{
	SOCKET fd = s->c.sockbase.fd;
	if (fd != INVALID_SOCKET) {
		sockclose(fd);
		s->c.sockbase.fd = INVALID_SOCKET;
	}
}

void server_free(struct server *s)
{
	server_close(s);
	obj_release(s->env);
	free(s);
}

static void server_free_sockbase(struct sockbase *base)
{
	struct connection *c = container_of(base, struct connection, sockbase);
	struct server *s = container_of(c, struct server, c);
	server_free(s);
}

/* returns 0 if we would block or -1 on error */
static int server_object_run(struct server *s)
{
	int tries = 10;
	do {
		const char *current_name = obj_get(s->env, "current");
		if (!current_name)
			return -1;
		struct object *current_object = object_load(current_name);
		if (!current_object)
			return -1;
		struct act_context ctx = {
			.env = s->env,
			.o = current_object,
			.c = &s->c,
			.state = s->tt_state,
		};
		const char *action = obj_get(current_object, "action");
		const char *next = obj_get(current_object, "next");
		fprintf(stderr, "%s():%d:[fd%d] current=\"%s\" tts=%d action=\"%s\"\n", __func__, __LINE__, s->c.sockbase.fd, current_name, TT_LINE(&s->tt_state), action);
		obj_set(s->env, "current", next);
		int e = command_run(action, &ctx);
		s->tt_state = e; /* update the state */
		fprintf(stderr, "%s():%d:[fd%d] tts=%d buflen=%d\n", __func__, __LINE__, s->c.sockbase.fd, TT_LINE(&s->tt_state), s->c.buflen);
		action = current_name = next = NULL; /* no long valid */
		obj_release(current_object);
		if (e != ACT_DONE) {
			fprintf(stderr, "INFO:%s():%s on %s\n", __func__, e > 0 ? "blocking" : "error", obj_get(s->env, "current"));
			return 0; /* we would block */
		}
	} while (--tries);
	return 0; /* treat deep recursion as blocking */
}

static void iac_will(struct connection *c, unsigned char telopt)
{
	unsigned char m[3] = { 255, 251, telopt }; /* IAC WILL <telopt> */
	connection_write(c, m, sizeof(m));
	fprintf(stderr, "%s():%d:[fd%d] sent: IAC WILL %hhu\n",
		__func__, __LINE__, c->sockbase.fd, telopt);
}

static void iac_wont(struct connection *c, unsigned char telopt)
{
	unsigned char m[3] = { 255, 252, telopt }; /* IAC WONT <telopt> */
	connection_write(c, m, sizeof(m));
	fprintf(stderr, "%s():%d:[fd%d] sent: IAC WONT %hhu\n",
		__func__, __LINE__, c->sockbase.fd, telopt);
}

static void iac_do(struct connection *c, unsigned char telopt)
{
	unsigned char m[3] = { 255, 253, telopt }; /* IAC DO <telopt> */
	connection_write(c, m, sizeof(m));
	fprintf(stderr, "%s():%d:[fd%d] sent: IAC DO %hhu\n",
		__func__, __LINE__, c->sockbase.fd, telopt);
}

static void iac_dont(struct connection *c, unsigned char telopt)
{
	unsigned char m[3] = { 255, 254, telopt }; /* IAC DONT <telopt> */
	connection_write(c, m, sizeof(m));
	fprintf(stderr, "%s():%d:[fd%d] sent: IAC DONT %hhu\n",
		__func__, __LINE__, c->sockbase.fd, telopt);
}

/* Response to every DONT with WONT */
static void on_telnet_dont(struct connection *c, void *p, unsigned char telopt)
{
	iac_wont(c, telopt);
}

static void server_event(SOCKET fd, struct sockbase *sockbase, long event)
{
	struct connection *c = container_of(sockbase, struct connection, sockbase);
	struct server *s = container_of(c, struct server, c);
	static __thread char decode_buf[1048576];

	if (event & EVENT_WRITE) {
		if (c->outbuf_len) {
			int e = write(fd, c->outbuf, c->outbuf_len);
			if (e < 0) {
				sockerror("write()");
				server_close(s);
				return;
			}
			if (e < (int)c->outbuf_len) {
				c->outbuf_len -= e;
				memmove(c->outbuf, c->outbuf + e, c->outbuf_len);
			} else {
				/* e == c->outbuf_len; don't bother doing a 0-length memmove() */
				c->outbuf_len = 0;
			}
		}
		/* if the buffer is empty, clear the write flag */
		if (!c->outbuf_len)
			sockclr(fd, EVENT_WRITE);
	}
	if (event & EVENT_READ) {
		int rem = (int)c->bufmax - (int)c->buflen;
		if (rem > (int)sizeof(decode_buf))
			rem = sizeof(decode_buf);
		if (rem > 0) {
			/* read TELNET into a buffer */
			int e = read(fd, decode_buf, rem);
			if (e < 0) {
				sockerror("read()");
				server_close(s);
				return;
			}
			if (e == 0) {
				fprintf(stderr, "Connection closed\n");
				server_close(s);
				return;
			}

			/* Decode telnet stream (NVT) into connection's buffer */
			struct telnet_context ctx = {
				.c = c,
				.p = s,
				.f_dont = on_telnet_dont,
			};
			int len = telnet_parse(&c->telnet, &ctx, decode_buf, e, c->buf + c->buflen, rem);
			c->buflen += len;

			/* assuming we were left in blocking state, let the object actions attempt to run again */
			e = server_object_run(s);
			if (e == -1) {
				/* discard all input if something weird happened */
				fprintf(stderr, "TODO:process string:\"%.*s\" [%d]\n", c->buflen, c->buf, c->buflen);
				c->buflen = 0;
			}
		}
	}
}

static struct sockbase *server_new(SOCKET fd, const char *origin)
{
	struct server *s;
	s = calloc(1, sizeof(*s));
	RETAIN(&s->c.sockbase);
	connection_init(&s->c, fd);
	TT_INITIALIZE(&s->tt_state);

	/* copy the template environment */
	const char *template = obj_get(system_env, "server.template");
	if (template) {
		s->env = object_load(template);
	} else {
		fprintf(stderr, "WARNING:server.template not set, using empty environment\n");
		s->env = obj_new();
	}

	/* set environment variable */
	if (obj_set(s->env, "ORIGIN", origin)) {
		struct sockbase *sb = &s->c.sockbase;
		fprintf(stderr, "ERROR:could not create connection\n");
		RELEASE(sb, server_free_sockbase);
		return NULL;
	}

	sockadd(fd, &s->c.sockbase, EVENT_READ, server_event, server_free_sockbase);

	/* show an annoying legal notice */
	connection_printf(&s->c,
		"The Waking Well - "
		"Copyright 2015-2017 Jon Mayo <jon@cobra-kai.com>\n"
		"\n"
	);

//	connection_write(&s->c, "\377\375\30", 3); /* IAC DO TERMINAL-TYPE */
//	connection_write(&s->c, "\377\372\30\1\377\360", 6); /* IAC SB TERMINAL-TYPE SEND IAC SE */

//	iac_wont(&s->c, 34); /* IAC WONT LINEMODE */
//	iac_wont(&s->c, 1); /* IAC WONT ECHO */
//	iac_will(&s->c, 10); /* IAC WILL NAOCRD */
//	iac_will(&s->c, 42); /* IAC WILL CHARSET */

	// TODO: only if LINEMODE worked
	// connection_write(&s->c, "\377\372\42\1\1\377\360", 7); /* IAC SB LINEMODE MODE edit IAC SE */

	// TODO: only if NAOCRD worked
	// connection_write(&s->c, "\377\372\12\1\374\377\360", 7); /* IAC SB NAOCRD DR 252 IAC SE */

	// TODO: handle only if CHARSET negotiation worked
	// connection_write(&s->c, "\377\372\52\2UTF-8\377\360", 11); /* IAC SB CHARSET ACCEPT "UTF-8" IAC SE */

	/*
	struct act_context ctx = {
		.env = s->env,
		.o = object_load("system/welcome"),
		.c = &s->c,
	};
	command_run("print", &ctx);
	*/

	obj_set(s->env, "current", "system/welcome");
	server_object_run(s);

	return &s->c.sockbase;
}

/******************************************************************************/

/* a service accepts connections from remote client and creates servers */
struct service {
	struct sockbase sockbase;
	struct service *next, **prev;
	struct object *template; // TODO: load this
};

static struct service *service_list;

static void service_close(struct service *s)
{
	sockclose(s->sockbase.fd);
	s->sockbase.fd = -1;
}

void service_free(struct service *s)
{
	service_close(s);
	free(s);
}

static void service_free_sockbase(struct sockbase *base)
{
	struct service *s = container_of(base, struct service, sockbase);
	service_free(s);
}

static void service_event(SOCKET fd, struct sockbase *sockbase, long event)
{
	// TODO: setup global parameters from the service structure:
	struct service *s = container_of(sockbase, struct service, sockbase);

	if (event & EVENT_READ) {
		struct sockaddr_storage sa;
		socklen_t sa_len = sizeof(sa);

		SOCKET newfd = accept4(fd, (struct sockaddr*)&sa, &sa_len, SOCK_NONBLOCK | SOCK_CLOEXEC);
		if (newfd == INVALID_SOCKET) {
			if (errno != EAGAIN)
				sockerror("accept()");
			return;
		}

		/* post <host>/<port> into buffer host[] ... */
		char host[128], port[64];
		int e = getnameinfo((struct sockaddr*)&sa, sa_len,
			host, sizeof(host), port, sizeof(port),
			NI_NUMERICHOST | NI_NUMERICSERV);
		if (e) {
			snprintf(host, sizeof(host), "<UNKNOWN-HOST>");
			snprintf(port, sizeof(port), "<UNKNOWN-PORT>");
		}
		size_t hostlen = strlen(host);
		// TODO: check for truncation in snprintf
		snprintf(host + hostlen, sizeof(host) - hostlen, "/%s", port);

		struct sockbase *newserver = server_new(newfd, host);
		if (!newserver) {
			fprintf(stderr, "ERROR:could not create connection\n");
			sockclose(newfd);
			return;
		}

		fprintf(stderr, "New connection: %s\n", host);
	}
}

int service_open(const char *hostport)
{
	/* split host and port number from HHHHH/NNNN */
	char host[NI_MAXHOST];
	const char *service = strrchr(hostport, '/');
	if (!service)
		service = hostport;
	snprintf(host, sizeof(host), "%.*s", (int)(service - hostport), hostport);
	if (service)
		service++;

	/* Bind all matching addresses */
	struct addrinfo *res, *cur, hints = {
		.ai_flags = AI_NUMERICHOST | AI_PASSIVE,
		.ai_family = AF_INET,
		.ai_socktype = SOCK_STREAM,
	};
	int e = getaddrinfo(*host ? host : NULL, service, &hints, &res);
	if (e) {
		fprintf(stderr, "ERROR:%s:%s\n", hostport, gai_strerror(e));
		return -1;
	}

	for (cur = res; cur; cur = cur->ai_next) {
		SOCKET fd = socket(cur->ai_family, cur->ai_socktype | SOCK_CLOEXEC | SOCK_NONBLOCK, cur->ai_protocol);
		if (fd == INVALID_SOCKET) {
			sockerror(hostport);
			continue;
		}
		/* fcntl(fd, F_SETFL, O_NONBLOCK); */
		/* fcntl(fd, F_SETFD, FD_CLOEXEC); */

		if (bind(fd, cur->ai_addr, cur->ai_addrlen) == -1 ||
				listen(fd, 6) == -1) {
			sockerror(hostport);
			sockclose(fd);
			continue;
		}

		/* add the service */
		struct service *s;
		s = calloc(1, sizeof(*s));
		RETAIN(&s->sockbase); // TODO: write a function to close a service too
		s->sockbase.fd = fd;
		DLIST_INSERT_AFTER(&service_list, s);
		sockadd(fd, &s->sockbase, EVENT_READ, service_event, service_free_sockbase);
		fprintf(stderr, "Started %s\n", hostport);
	}
	freeaddrinfo(res);
	return 0;
}

/******************************************************************************/

int main(int argc, char **argv)
{
	setlocale(LC_ALL, NULL);

	/* parse command-line options */
	int e = 0;
	switch (argc) {
	case 1:
		e = filedb_set_root("./db");
		break;
	case 2:
		e = filedb_set_root(argv[1]);
		break;
	default:
		fprintf(stderr, "usage: %s [<dbpath>]\n", basename(argv[0]));
		return EXIT_FAILURE;
	}
	if (e) {
		fprintf(stderr, "unable to configure DB path\n");
		return EXIT_FAILURE;
	}

	/* load enviroment options */
	system_env = object_load("system/config");
	if (!system_env) {
		fprintf(stderr, "ERROR:system/config not found.\n");
		return EXIT_FAILURE;
	}

	actions_register();

	service_open("/5000"); // TODO: read from system_env
	while (sockets_count > 0) {
		if (sockpoll()) {
			return EXIT_FAILURE;
		}
	}

	obj_release(system_env);
	system_env = NULL;
	return 0;
}
