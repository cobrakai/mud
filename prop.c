/* Copyright (c) 2016-2017, Jon Mayo <jon@cobra-kai.com> */
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <search.h>

#include "grow.h"
#include "prop.h"

//// Property Lists ////

property_t
prop_new(const char *name, const char *value)
{
	if (!name) /* unnamed properties are not valid */
		return NULL;
	if (!value) /* treat NULL as an empty string */
		value = "";
	size_t n = strlen(name) + 1, v = strlen(value) + 1;
	property_t p = malloc(n + v);
	if (!p)
		return NULL; /* allocation error */
	memcpy(p, name, n);
	memcpy(p + n, value, v);
	return p;
}

void
plist_free(struct property_list *plist)
{
	unsigned i;
	unsigned len = plist->len;
	for (i = 0; i < len; i++) {
		property_t *ptr = &plist->list[i];
		prop_free(*ptr);
		*ptr = NULL;
	}
	free(plist->list);
}

/* compare two property names */
static int
prop_compar(const void *a, const void *b)
{
        return strcmp(*(property_t *)a, *(property_t *)b);
}

/* return offset or -1 on error. */
int
prop_find_slot(property_t *prop, unsigned prop_len, const char *name)
{
        if (!prop_len)
                return -1; /* no match because list is empty */

        property_t *res = bsearch(&name, prop, prop_len,
		sizeof(*prop), prop_compar);

        if (res)
                return res - prop;

        return -1; /* no match */
}

int
plist_set(struct property_list *plist, const char *name, const char *value)
{
	property_t p = prop_new(name, value);
	if (!p)
		return -1;

        int ofs = prop_find_slot(plist->list, plist->len, name);
        if (ofs >= 0) {
		property_t *ptr = &plist->list[ofs];
		/* entry was found, replace into the same slot */
                free(*ptr);
                *ptr = p;
		/* no need to qsort() because name hasn't changed */
                return 0;
        }

	/* else add a new entry on the end */
	ofs = plist->len;
	unsigned new_size = plist->len + 1;
	if (new_size >= plist->max) { /* make space for the entry */
		if (grow(&plist->list, &plist->max, new_size,
				sizeof (*plist->list))) {
			prop_free(p);
			return -1; /* allocation error */
		}
	}
	plist->len = new_size;
	plist->list[ofs] = p;

	/* the bsearch() requires the array to be sorted */
	qsort(plist->list, plist->len, sizeof(*plist->list), prop_compar);

	return 0;
}

const char *
plist_get(struct property_list *plist, const char *name)
{
        int ofs = prop_find_slot(plist->list, plist->len, name);
	if (ofs < 0)
		return NULL;
	property_t p = plist->list[ofs];
	return prop_get_value(p);
}

/* gets the next property name-value pair.
 * return false when there are no more properties.
 * the base pointer passed through iter should not be modified between calls of
 * this function.
 */
bool
prop_iter_next(struct prop_iter *iter,
	const char **name, const char **value)
{
	int i = iter->i;
	if (i >= iter->max)
		return false;
	const property_t p = iter->base[i];
	if (name)
		*name = prop_get_name(p);
	if (value)
		*value = prop_get_value(p);
	iter->i++;
	return true;
}
