# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

    It should be as simple as running `make`

* Configuration
* Dependencies

    A POSIX C environment is all that is needed. There are no dependencies on third party libraries.

* Database configuration

    TBD: it's complicated

* How to run tests

    Run the command-line test. No options normally need to be provided to execute the full test. Example: `./test_object`
 
* Deployment instructions

### Contribution guidelines ###

* Writing tests

    Tests are required for each major component. They should start with the word `test_` and return success or failure based on the results.
    Tests should be built by default. This can help catch compilation errors and aids with refactoring workflow.

* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact