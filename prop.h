//// Property Lists ////
#ifndef PROP_H
#define PROP_H
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

typedef char *property_t;

struct prop_iter {
	property_t *base;
	int i;
	int max;
};

struct property_list {
	unsigned len, max;
	property_t *list; /* entries are stored as "key\0value\0" */
};

static inline const char *
prop_get_name(const property_t p)
{
	return p;
}

static inline const char *
prop_get_value(const property_t p)
{
	return p + strlen(p) + 1;
}

static inline void
prop_free(property_t p)
{
	free(p);
}

/* initializes a prop_iter for iterating through an array of property_t.
 * the base pointer passed through iter should not be modified between calls of
 * this function or prop_iter_next(). */
static inline void
prop_iter_init(struct prop_iter *iter, property_t *base, unsigned max)
{
	iter->base = base;
	iter->i = 0;
	iter->max = max;
}

property_t prop_new(const char *name, const char *value);
void plist_free(struct property_list *plist);
int prop_find_slot(property_t *prop, unsigned prop_len, const char *name);
int plist_set(struct property_list *plist, const char *name, const char *value);
const char *plist_get(struct property_list *plist, const char *name);
bool prop_iter_next(struct prop_iter *iter, const char **name, const char **value);

#endif
