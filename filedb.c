/* Copyright (c) 2016-2017, Jon Mayo <jon@cobra-kai.com> */
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "log.h"
#include "filedb.h"

/* this path will be relative to the filedb_root.
 * the routine assumes the last 6 characters will be overwritten by a RNG.
 */
#define FILEDB_TEMP_PATTERN "/tmp/obj.XXXXXX"

struct filedb {
	FILE *f;
	int fd;
	char *filename;
	char *tempname;
	struct stat statbuf;
};

static char *filedb_root = NULL; /* this is no default path */
static int filedb_fd = -1; /* use this directory for all openat() calls */

/* filedb_root_check() opens the root path.
 * once this is called, filedb_set_root() can no longer be used.
 * return -1 on error, 0 on success */
static int
filedb_root_check(void)
{
	if (filedb_fd == -1) {
		if (!filedb_root) {
			log_error("%s():please configure DB path\n", __func__);
			return -1;
		}
		filedb_fd = open(filedb_root, O_DIRECTORY);
	}

	if (filedb_fd == -1) {
		log_syserror(filedb_root);
		return -1;
	}

	return 0;
}

/* create parent directory components for a file path */
static int
filedb_create_components(const char *path)
{
	int cwd = dup(filedb_fd);
	char sub[PATH_MAX];

	const char *end;
	while ((end = strchr(path, '/')) != NULL) {
		size_t sublen = end - path;
		if (!sublen)
			break;
		if (sublen > sizeof (sub) - 1)
			goto error;
		memcpy(sub, path, sublen);
		sub[sublen] = 0;
		path = end + 1;
		if (mkdirat(cwd, sub, 0777) == -1 && errno != EEXIST)
			goto error;
		int next = openat(cwd, sub, O_DIRECTORY);
		close(cwd);
		cwd = next;
	}
	close(cwd);
	return 0;
error:
	log_syserror(path);
	close(cwd);
	return -1;
}

/* objdb_temp() create a stream to store an object.
 * later the path stored in tempname will be renamed by filedb_commit() to the
 * target location. tempname is assumed to be PATH_MAX in size. */
static FILE *
filedb_temp(char *tempname)
{
	if (filedb_root_check())
		return NULL;

	/* this routine is hardcoded to have a 6 digit pattern XXXXXX */
	int e = snprintf(tempname, PATH_MAX, FILEDB_TEMP_PATTERN);
	if (e < 0 || e >= PATH_MAX) {
		log_syserror(FILEDB_TEMP_PATTERN);
		errno = EINVAL;
		return NULL;
	}

	int tmpofs = strlen(tempname) - 6;
	if (tmpofs <= 0) {
		log_error("%s:illegal temp pattern\n", FILEDB_TEMP_PATTERN);
		errno = EINVAL;
		return NULL;
	}

	if (filedb_create_components(tempname) == -1) {
		log_error("%s:unable to create destination path\n", tempname);
		errno = EINVAL;
		return NULL;
	}

	int fd;
	unsigned long seq = time(0);
	int tries = 1000;
	do {
		/* generate a sequence number using a simple Lehmer RNG */
		seq = (16807UL * seq) % 2147483647UL;
		snprintf(tempname + tmpofs, 6, "%06u", rand() % 1000000);
		/* these temp files are always exclusive */
		int oflags = O_CREAT | O_EXCL | O_RDWR;
		errno = 0;
		fd = openat(filedb_fd, tempname, oflags, 0666);
		if (fd < 0) {
			if (errno == EEXIST) {
				seq = 13 * tries * time(0); /* leave the sequence on collision */
				continue;
			} else {
				log_syserror(tempname);
				return NULL;
			}
		}
	} while (fd < 0 && tries--);
	if (fd < 0) {
		log_error("%s():unable to open a temp file\n", __func__);
		return NULL;
	}

	return fdopen(fd, "r+");
}

/* filedb_set_root configures the DB path.
 * returns 0 on success, and -1 on failure. */
int
filedb_set_root(const char *path)
{
	if (filedb_fd != -1 || !path) {
		/* we don't permit this because some txn might be outstanding
		 * and weird things could potentially happen, like the /tmp
		 * file and the target file could end up in different
		 * directories. -jon */
		log_error("%s():changing DB path not permitted after initialization\n", __func__);
		return -1;
	}
	free(filedb_root);
	filedb_root = strdup(path);
	return filedb_root_check();
}

FILE *
filedb_file(struct filedb *h)
{
	return h->f;
}

int
filedb_fileno(struct filedb *h)
{
	return h->fd;
}

const char *
filedb_get_root(void)
{
	return filedb_root;
}

static void
filedb_free(struct filedb *h)
{
	if (h->f)
		fclose(h->f);
	h->fd = -1;
	free(h->tempname);
	free(h->filename);
	free(h);
}

/* open a write-only transaction */
struct filedb *
filedb_open(const char *path)
{
	struct filedb *h = calloc(1, sizeof (*h));
	char tempname[PATH_MAX];
	FILE *f = filedb_temp(tempname);
	if (!f)
		goto fail;
	h->f = f;
	h->fd = fileno(f);
	h->tempname = strdup(tempname);
	h->filename = strdup(path);
	return h;
fail:
	free(h);
	return NULL;
}

struct filedb *
filedb_openfmt(const char *fmt, ...)
{
	char path[PATH_MAX];
	va_list ap;
	va_start(ap, fmt);
	int n = vsnprintf(path, sizeof(path), fmt, ap);
	va_end(ap);
	if (n <= 0 || n >= (int)sizeof(path)) {
		errno = EINVAL;
		return NULL;
	}
	return filedb_open(path);
}

/* discards the transcation (deletes the temp file) */
int
filedb_rollback(struct filedb *h)
{
	if (filedb_root_check())
		return -1;

	int e = unlinkat(filedb_fd, h->tempname, 0);
	if (e)
		log_syserror(h->tempname);
	filedb_free(h);
	return e;
}

/* renames the temp file to the destination file */
int
filedb_commit(struct filedb *h)
{
	if (filedb_root_check())
		return -1;

	filedb_create_components(h->filename);

	int e = renameat(filedb_fd, h->tempname, filedb_fd, h->filename);
	if (e)
		log_syserror(h->filename);
	filedb_free(h);
	return e;
}

/* open a read-only stream */
FILE *
filedb_open_readonly(const char *path)
{
	if (filedb_root_check())
		return NULL;
	int fd = openat(filedb_fd, path, O_RDONLY);
	if (fd == -1) {
		log_syserror(path);
		return NULL;
	}
	return fdopen(fd, "r");
}
