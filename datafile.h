//// Data file format parser ////
#ifndef DATAFILE_H
#define DATAFILE_H

#include <stdio.h>

struct datafile;

enum datafile_event {
	D_ERROR,
	D_DONE,
	D_OBJECT_OPEN,
	D_OBJECT_CLOSE,
	D_PROPERTY_NAME,
	D_PROPERTY_VALUE,
};

/* input routines */
struct datafile *datafile_openfile(FILE *f, const char *filename);
void datafile_close(struct datafile *ds);
enum datafile_event datafile_next(struct datafile *ds);
const char *datafile_get_string(struct datafile *ds);
const char *datafile_filename(struct datafile *ds);
int datafile_line(struct datafile *ds);

/* output routines */
int datafile_write_start(FILE *f, const char *filename, const char *tag);
int datafile_write_property(FILE *f, const char *filename, const char *name, const char *value);
int datafile_write_end(FILE *f, const char *filename);
#endif
