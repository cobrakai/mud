/* Copyright (c) 2016-2017, Jon Mayo <jon@cobra-kai.com> */
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include "grow.h"

//// String Library ////

struct string
{
	unsigned cur; /* cursor position for append */
	unsigned len; /* current allocated size */
	unsigned max; /* maximum allocated size */
	char *s;
};

struct string *
string_new(unsigned maximum_size)
{
	struct string *s = malloc(sizeof(*s));
	s->len = 0;
	s->len = 0;
	s->max = maximum_size ? maximum_size : INT_MAX;
	s->s = NULL;
	return s;
}

/* grows a string to fit new_size.
 * does not count null terminator.
 * returns failure if max is exceeded or there is an allocation failure. */
int
string_grow(struct string *s, unsigned new_size)
{
	if (new_size >= s->max)
		return -1;
	return grow(&s->s, &s->len, new_size + 1, 1);
}

/* returns zero if the size increase would be successful.
 * new_size = old_size + increase; */
static inline int
string_check(struct string *s, unsigned increase)
{
	return s->cur + 1 + increase <= s->len ? 0 : -1;
}

int
string_addch(struct string *s, char ch)
{
	unsigned cur = s->cur;
	if (string_check(s, 1))
		if (string_grow(s, s->cur + 1))
			return -1;
	s->s[cur] = ch;
	s->s[cur + 1] = 0;
	s->cur += 1;
	return 0;
}

/* add unicode character as UTF-8 encoded */
int
string_addwch(struct string *s, unsigned long w) {
	if (w < 0x80) {
		return string_addch(s, w);
	} else if (w < 0x800) {
		string_addch(s, 0xc0 | (w >> 6));
		return string_addch(s, 0x80 | (w & 0x3f));
	} else if (w < 0x10000) {
		string_addch(s, 0xe0 | (w >> 12));
		string_addch(s, 0x80 | ((w >> 6) & 0x3f));
		return string_addch(s, 0x80 | (w & 0x3f));
	} else if (w < 0x200000) { /* technically should be 0x120000 */
		string_addch(s, 0xf0 | (w >> 18));
		string_addch(s, 0x80 | ((w >> 12) & 0x3f));
		string_addch(s, 0x80 | ((w >> 6) & 0x3f));
		return string_addch(s, 0x80 | (w & 0x3f));
	}
	/* five and six byte encodings aren't part of the spec, even though it is obvious how to do them. */
	return -1;
}

/* Initialize from a null-terminated C string.
 * return positive length on success.
 * return negative on failure.
 */
int
string_set(struct string *s, const char *str)
{
	unsigned len;
	if (!str)
		len = 0;
	else
		len = strlen(str);
	s->cur = len;
	if (string_grow(s, len))
		return -1;
	if (str)
		memcpy(s->s, str, len + 1);
	else
		*s->s = 0;
	return len;
}

/* return a null-terminated C string. */
const char *
string_get(struct string *s)
{
	return s->s;
}

/* adds b to the end of s.
 */
int
string_append(struct string *s, struct string *b)
{
	unsigned len = s->cur + b->cur;
	if (string_grow(s, len + 1))
		return -1;
	if (b->cur)
		memcpy(s->s + s->cur, b->s, b->cur);
	s->cur = len;
	s->s[len] = 0;
	return 0;
}

/* duplicates a string, but shrinks max to the minimum needed. */
struct string *
string_dup(struct string *s)
{
	struct string *r = string_new(s->max);
	if (string_grow(r, s->cur))
		return NULL;
	assert(string_check(r, s->cur) == 0);
	r->cur = s->cur;
	memcpy(r->s, s->s, s->cur + 1);
	return r;
}

void
string_free(struct string *s)
{
	if (s) {
		free(s->s);
		s->s = NULL;
		s->cur = s->len = s->max = 0;
		free(s);
	}
}

void test_string(void)
{
	struct string *a = string_new(0);
	struct string *b = string_new(12);
	struct string *c = string_new(4);
	struct string *d = string_new(0);
	int i;

	for (i = 0; i < 10; i++) {
		string_addch(c, '!');
	}

	string_set(a, "This is a test");
	string_append(b, a);
	string_append(b, a);
	string_append(b, c);

	string_addch(d, 'X');
	string_free(d);
	d = string_dup(b);

	string_free(d);
	string_free(c);
	string_free(b);
	string_free(a);

	fprintf(stderr, "%s():test passed\n", __func__);
}

//// End of String ////
