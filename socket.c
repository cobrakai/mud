/* Copyright 2015-2017 Jon Mayo <jon@cobra-kai.com>
 *
 * Permission to use, copy, modify, or distribute this software for any
 * purpose with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/******************************************************************************/
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <sys/select.h>
#include <unistd.h>

#include "socket.h"
#include "rc.h"

#define SOCKMAX 256

static struct socket_info {
	SOCKET fd; /* this is redundant because of sockbase */
	struct sockbase *ptr;
} sockets[SOCKMAX];
static int sockets_max = sizeof(sockets) / sizeof(*sockets);
static int sockets_fdmax;
static fd_set sockets_rfds, sockets_wfds;

int sockets_count;

void sockerror(const char *reason)
{
	fprintf(stderr, "%s:%s\n", reason, strerror(errno));
}

void sockclose(SOCKET fd)
{
	if (fd != INVALID_SOCKET) {
		FD_CLR(fd, &sockets_rfds);
		FD_CLR(fd, &sockets_wfds);
		close(fd);
		sockets_count--;
	} else {
		fprintf(stderr, "%s:fd is invalid!\n", __func__);
	}
}

int sockset(SOCKET fd, long events)
{
	if (fd == INVALID_SOCKET || fd >= sockets_max)
		return -1;
	if (events & EVENT_READ)
		FD_SET(fd, &sockets_rfds);
	if (events & EVENT_WRITE)
		FD_SET(fd, &sockets_wfds);
	return 0;
}

int sockclr(SOCKET fd, long events)
{
	if (fd == INVALID_SOCKET || fd >= sockets_max)
		return -1;
	if (events & EVENT_READ)
		FD_CLR(fd, &sockets_rfds);
	if (events & EVENT_WRITE)
		FD_CLR(fd, &sockets_wfds);
	return 0;
}

int sockadd(SOCKET fd, struct sockbase *ptr, long events,
	void (*event)(SOCKET fd, struct sockbase *ptr, long event),
	void (*free)(struct sockbase *ptr))
{
	if (fd == INVALID_SOCKET || fd >= sockets_max)
		return -1;
	if (fd > sockets_fdmax)
		sockets_fdmax = fd;
	sockets_count++;
	sockets[fd].fd = fd; /* this is somewhat redundant */
	sockets[fd].ptr = ptr;
	ptr->event = event;
	ptr->free = (void(*)(void*))free;
	sockset(fd, events);
	return 0;
}

int sockpoll(void)
{
	if (!sockets_count)
		return -1;

	fd_set rfds = sockets_rfds;
	fd_set wfds = sockets_wfds;

	struct timeval next = { .tv_sec = 300 }; // TODO: find next timer
	int n = select(sockets_fdmax + 1, &rfds, &wfds, NULL, &next);
	if (n < 0) {
		sockerror("select()");
		return -1;
	}

	int i;
	for (i = 0; i < sockets_max; i++) {
		if (sockets[i].ptr == NULL)
			continue;
		int fd = sockets[i].fd;
		if (fd == INVALID_SOCKET)
			continue;
		int event = 0;
		if (FD_ISSET(fd, &rfds))
			event |= EVENT_READ;
		if (FD_ISSET(fd, &wfds))
			event |= EVENT_WRITE;

		if (event) {
			struct sockbase *s = sockets[i].ptr;
			RETAIN(s);
			s->event(fd, sockets[i].ptr, event);
			RELEASE(s, s->free);
		}
	}

	return 0;
}

/******************************************************************************/
