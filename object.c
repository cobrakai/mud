/* Copyright (c) 2016-2017, Jon Mayo <jon@cobra-kai.com> */
#include <string.h>
#include <stdlib.h>
#include <search.h>
#include <stdbool.h>
#include "grow.h"
#include "prop.h"
#include "datafile.h"
#include "rc.h"
#include "log.h"
#include "filedb.h"
#include "object.h"

//// Object Core ////

struct object {
	bool archived_flag:1; /* 1 if saved since last modification */
	bool cached_flag:1; /* 1 if loaded from cache */
	int rc; /* reference count */
	char *id;
	struct property_list plist;
};

void
object_free(struct object *o)
{
	plist_free(&o->plist);
	free(o->id);
	free(o);
}

static inline void
clear_archived_flag(struct object *o)
{
	o->archived_flag = false;
}

static inline void
set_archived_flag(struct object *o)
{
	o->archived_flag = true;
}

struct object *
object_new()
{
	struct object *o = calloc(1, sizeof (*o));
	o->rc = RC_INIT;
	RETAIN(o); /* new objects have one extra count */
	return o;
}

void
object_retain(struct object *o)
{
	RETAIN(o);
}

void
object_release(struct object *o)
{
	RELEASE(o, object_free);
}

int
object_set_id(struct object *o, const char *id)
{
	free(o->id);
	o->id = id ? strdup(id) : NULL;
	clear_archived_flag(o);
	return 0;
}

const char*
object_get_id(const struct object *o)
{
	return o && o->id ? o->id : NULL;
}

const char *
object_prop_get(struct object *o, const char *name)
{
	return plist_get(&o->plist, name);
}

int
object_prop_set(struct object *o, const char *name, const char *value)
{
	clear_archived_flag(o);
	return plist_set(&o->plist, name, value);
}

/* sets up a prop_iter from an object */
void
object_prop_iter_init(const struct object *o, struct prop_iter *iter)
{
	prop_iter_init(iter, o->plist.list, o->plist.len);
}

//// End Of Object Core ////

//// Object Store ////

/* This function is limited to 1 object per file */
struct object *
object_load(const char *filename)
{
	FILE *f = filedb_open_readonly(filename);
	if (!f)
		goto failure;
	struct object *obj = object_new();
	struct datafile *ds = datafile_openfile(f, filename);
	enum datafile_event ev;
	while ((ev = datafile_next(ds)) != D_DONE) {
		// fprintf(stderr, "DEBUG:%s():ev=%d\n", __func__, ev);
		if (ev == D_ERROR)
			goto failure_cleanup;
		switch (ev) {
		case D_OBJECT_OPEN: {
			const char *s = datafile_get_string(ds);
			object_set_id(obj, s);
			s = NULL;
			break;
		}
		case D_OBJECT_CLOSE:
			if ((ev = datafile_next(ds) == D_DONE))
				goto done;
			// the only thing that can follow an object is D_DONE
			// maybe one day we will allow multiple object definitions in a row
			goto failure_cleanup;
		case D_PROPERTY_NAME: {
			const char *tmp_name = datafile_get_string(ds);
			if (!tmp_name)
				goto failure_cleanup;
			/* return of datafile_get_string() valid until next call of datafile_next() */
			char *new_name = strdup(tmp_name);
			if ((ev = datafile_next(ds) != D_PROPERTY_VALUE)) {
				// property must be a name-value pair
				free(new_name);
				goto failure_cleanup;
			}
			const char *value = datafile_get_string(ds);
			if (!value) {
				free(new_name);
				goto failure_cleanup;
			}
			if (!new_name[0]) /* "" is not a valid key */ {
				log_error("empty key \"\" is not a valid key\n");
				free(new_name);
				goto failure_cleanup;
			}
			// DEBUG: log_debug("property:%s=%s\n", new_name, value);
			/* check if the property already exists */
			if (object_prop_get(obj, new_name)) {
				log_warn("%s:%d:duplicate property '%s'\n",
					datafile_filename(ds),
					datafile_line(ds), new_name);
			}
			object_prop_set(obj, new_name, value);
			free(new_name);
			break;
		}
		case D_ERROR:
		case D_DONE:
		case D_PROPERTY_VALUE:
			// should not occur in this loop
			goto failure_cleanup;
		}
	}
done:
	set_archived_flag(obj);
	datafile_close(ds);
	fclose(f);
	return obj;
failure_cleanup:
	datafile_close(ds);
	fclose(f);
	object_free(obj);
failure:
	log_error("%s:unable to load\n", filename);
	return NULL;
}

/* if filename is NULL, then use object id. */
int
object_save(struct object *o, const char *filename)
{
	const char *id = object_get_id(o);
	if (!id) {
		fprintf(stderr, "%s:cannot save object that has no ID set\n",
			filename);
		return -1;
	}

	if (!filename)
		filename = id;
	struct filedb *h = filedb_open(filename);
	if (!h) {
		perror(filename);
		return -1;
	}
	FILE *f = filedb_file(h);
	if (!f) {
		perror(filename);
		filedb_rollback(h);
		return -1;
	}

	datafile_write_start(f, filename, id);

	struct prop_iter iter;
	const char *name, *value;
	object_prop_iter_init(o, &iter);
	while (prop_iter_next(&iter, &name, &value)) {
		datafile_write_property(f, filename, name, value);
	}
	datafile_write_end(f, filename);

	if (ferror(f)) {
		fprintf(stderr, "%s:error saving file for object \"%s\"\n",
			filename, id);
		fclose(f);
		return -1;
	}

	set_archived_flag(o);
	filedb_commit(h);
	return 0;
}

int
object_dump(const struct object *o)
{
	const char *id = object_get_id(o);
	fprintf(stderr, "<<<START object dump>>>\n");
	datafile_write_start(stderr, "<STDERR>", id);
	struct prop_iter iter;
	const char *name, *value;
	object_prop_iter_init(o, &iter);
	while (prop_iter_next(&iter, &name, &value)) {
		datafile_write_property(stderr, "<STDERR>", name, value);
	}
	datafile_write_end(stderr, "<STDERR>");
	fprintf(stderr, "<<<END object dump>>>\n");
	return 0;
}

//// TODO: Object Cache ////
