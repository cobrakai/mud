/* Copyright 2015-2017 Jon Mayo <jon@cobra-kai.com>
 *
 * Permission to use, copy, modify, or distribute this software for any
 * purpose with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/******************************************************************************/
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "connection.h"
#include "cmd.h"
#include "object.h"
#include "rc.h"
#include "user.h"

/******************************************************************************/

/* store users in file at /users/[a-z]/<username> */
static int
user_path(char *path, size_t pathlen, const char *username)
{
	int len = snprintf(path, sizeof(path), "users/%c/%s", username[0], username);
	if (len >= (int)sizeof(path)) {
		errno = EINVAL;
		return -1; /* truncated */
	}
	return 0;
}

struct object *
user_load(const char *username)
{
	char path[64];

	if (user_path(path, sizeof(path), username))
		return NULL;

	struct object *user = object_load(path);
	if (!user)
		return NULL; /* not found */

	/* Do some validation on the object */

	const char *str = object_prop_get(user, "user");
	if (!str || strcasecmp(username, str)) {
		/* invalid object or name */
		obj_release(user);
		return NULL;
	}

	return user;
}

struct object *
user_create(const char *username)
{
	char path[64];

	if (user_path(path, sizeof(path), username))
		return NULL;

	struct object *user = object_new();
	if (!user)
		return NULL;
	object_set_id(user, path);
	object_prop_set(user, "user", username);
	return user;
}
