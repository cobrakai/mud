# global settings common to all components
CFLAGS += -Wall -W -g # -O3
CPPFLAGS += -D_GNU_SOURCE -Iinclude/

$(call StaticLibrary,object,object.c prop.c datafile.c filedb.c,)

$(call StaticLibrary,common,string-lib.c log.c grow.c socket.c,)

$(call StaticLibrary,network,socket.c,)

$(call StaticLibrary,user,user.c,)

$(call StaticLibrary,auth, \
	auth.c x86.S crypt_blowfish.c crypt_gensalt.c wrapper.c \
	,-D__SKIP_GNU)

$(call Executable,useradd,useradd.c,auth user object common,,,)

$(call Executable,test_object, \
	test_object.c,object common,,)

# $(call Executable,well,\
#	well.o socket.o telnet.o connection.o \
#	act.o cmd.o, \
#	auth user object common, \
#	,,-lm)

$(call SubDir,telnet-server)
