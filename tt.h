/* tt.h - Tiny Threads */
/* PUBLIC DOMAIN - Jon Mayo - August 23, 2005
 * Updated December 1, 2015
 */
#ifndef TT_H
#define TT_H

typedef unsigned tt_state;

#define TT_STATUS_EXITED	        (tt_state)0
/* Function using TT_BEGIN macro must be passed a tt_state and will return the
 * updated tt_state. Reentry into the function required the previously returned
 * tt_state value to be given.
 */
#define TT_BEGIN(tts)		        \
	switch (*(tts)) { \
	restart: __attribute__((maybe_unused)); \
	case TT_STATUS_EXITED:
#define TT_END(tts)		        } TT_EXIT(tts);
#define TT_INITIALIZE(tts)	        (*(tts) = TT_STATUS_EXITED)
#define TT_INITIALIZER		        {0}

/* wait while condition cond is true */
#define TT_WAIT_WHILE(tts, cond)	do { \
	case __LINE__: ; \
	if((cond)) { return (tt_state)__LINE__; } \
	} while(0)

/* checks condition cond and continues if it's true */
#define TT_WAIT_UNTIL(tts, cond)	do { \
	case __LINE__: ; \
	if(!(cond)) { return (tt_state)__LINE__; } \
	} while(0)

/* restarts the state machine from the beginning */
#define TT_RESTART(tts)		        do { \
	TT_INITIALIZE(tts); \
	goto restart; \
	} while(0)

/* clean exit of the function, a reset state will be used */
#define TT_EXIT(tts)		        do { \
	TT_INITIALIZE(tts); \
	return TT_STATUS_EXITED; } while(0)

/* use this for debugging, evalutes to the line number currently holding on */
#define TT_LINE(tts)		        (*(tts))

#endif
