//// Logging routines ////
#ifndef LOG_H
#define LOG_H
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>

#define log_assert(expr) do { \
	if (!(expr)) { \
		log_msg(LOG_ASSERT, __func__, "assert failure: " #expr); \
		abort(); \
	} } while (0)
#define log_error(...) log_msg(LOG_ERROR, __func__, __VA_ARGS__)
#define log_syserror(m) log_msg(LOG_ERROR, __func__, "%s:%s", (m), strerror(errno))

#define log_info(...) log_msg(LOG_INFO, __func__, __VA_ARGS__)
#define log_warn(...) log_msg(LOG_WARN, __func__, __VA_ARGS__)
#ifdef NDEBUG
#define log_debug(...) do { } while (0)
#define log_trace(...) do { } while (0)
#else
#define log_debug(...) log_msg(LOG_DEBUG, __func__, __VA_ARGS__)
#define log_trace(...) log_msg(LOG_TRACE, __func__, __VA_ARGS__)
#endif

enum log_level {
	LOG_ASSERT = 0, /**< unexpected condition forcing shutdown. */
	LOG_CRIT = 1, /**< critial message - system needs to shutdown. */
	LOG_ERROR = 2, /**< error occured - maybe fatal. */
	LOG_WARN = 3, /**< warning - something unexpected. */
	LOG_INFO = 4, /**< interesting information */
	LOG_TODO = 5, /**< messages for incomplete implementation. */
	LOG_DEBUG = 6, /**< debug messages. */
	LOG_TRACE = 7, /**< trace logging */
};

void log_format(char *buf, size_t buflen, enum log_level level, const char *tag, const char *fmt, va_list ap);
void log_msg(enum log_level level, const char *tag, const char *fmt, ...);
#endif
