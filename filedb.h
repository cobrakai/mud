#ifndef FILEDB_H
#define FILEDB_H
#include <stdio.h>
struct filedb;

int filedb_set_root(const char *path);
FILE *filedb_file(struct filedb *h);
int filedb_fileno(struct filedb *h);
const char *filedb_get_root(void);
struct filedb *filedb_open(const char *path);
struct filedb *filedb_openfmt(const char *fmt, ...);
int filedb_rollback(struct filedb *h);
int filedb_commit(struct filedb *h);
FILE *filedb_open_readonly(const char *path);
#endif
